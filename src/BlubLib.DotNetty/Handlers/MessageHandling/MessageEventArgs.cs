﻿using System;

namespace BlubLib.DotNetty.Handlers.MessageHandling
{
    public class MessageEventArgs : EventArgs
    {
        public object Message { get; }

        public MessageEventArgs(object message)
        {
            Message = message;
        }
    }
}
