﻿using System.Threading.Tasks;
using DotNetty.Transport.Channels;

namespace BlubLib.DotNetty.Handlers.MessageHandling
{
    public interface IMessageHandler
    {
        Task<bool> OnMessageReceived(IChannelHandlerContext context, object message);
    }
}