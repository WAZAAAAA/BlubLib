﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlubLib.Threading.Tasks;

namespace BlubLib.Collections.Generic
{
    public static class EnumerableExtensions
    {
        public static T FirstOfType<T>(this IEnumerable<object> @this)
        {
            return @this.OfType<T>().First();
        }

        public static T FirstOfTypeOrDefault<T>(this IEnumerable<object> @this)
        {
            return @this.OfType<T>().FirstOrDefault();
        }

        public static void ForEach<T>(this IEnumerable<T> @this, Action<T> callback)
        {
            foreach (var obj in @this)
                callback(obj);
        }

        public static void ForEach<T>(this IEnumerable<T> @this, Action<int, T> callbackWithIndex)
        {
            var i = 0;
            foreach (var obj in @this)
                callbackWithIndex(i++, obj);
        }

        /// <summary>
        /// Executes an async/await compatible callback on a <see cref="IEnumerable{T}"/>
        /// </summary>
        /// <returns>Returns an awaitable Task that is completed when the end of <see cref="@this"/> was reached</returns>
        /// <remarks>Callbacks are executed in order</remarks>
        public static async Task ForEachAsync<T>(this IEnumerable<T> @this, Func<T, Task> callback)
        {
            foreach (var obj in @this)
                await callback(obj).AnyContext();
        }

        /// <summary>
        /// Executes an async/await compatible callback on a <see cref="IEnumerable{T}"/>
        /// </summary>
        /// <returns>Returns an awaitable Task that is completed when the end of <see cref="@this"/> was reached</returns>
        /// <remarks>Callbacks are executed in order</remarks>
        public static async Task ForEachAsync<T>(this IEnumerable<T> @this, Func<int, T, Task> callbackWithIndex)
        {
            var i = 0;
            foreach (var obj in @this)
                await callbackWithIndex(i++, obj).AnyContext();
        }
    }

    public static class DictionaryExtensions
    {
        public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> @this, TKey key, TValue value)
        {
            if (@this.ContainsKey(key))
                return false;

            @this.Add(key, value);
            return true;
        }

        public static bool TryRemove<TKey, TValue>(this IDictionary<TKey, TValue> @this, TKey key, out TValue value)
        {
            return @this.TryGetValue(key, out value) && @this.Remove(key);
        }

        public static TValue AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> @this, TKey key,
            Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory)
        {
            if (@this.TryGetValue(key, out var oldValue))
            {
                var newValue = updateValueFactory(key, oldValue);
                @this[key] = newValue;
                return newValue;
            }

            var value = addValueFactory(key);
            @this.Add(key, value);
            return value;
        }

        public static TValue AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> @this, TKey key, TValue addValue,
            Func<TKey, TValue, TValue> updateValueFactory)
        {
            if (@this.TryGetValue(key, out var oldValue))
            {
                var newValue = updateValueFactory(key, oldValue);
                @this[key] = newValue;
                return newValue;
            }

            @this.Add(key, addValue);
            return addValue;
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> @this, TKey key)
        {
            return @this.TryGetValue(key, out var value) ? value : default(TValue);
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> @this, TKey key)
        {
            return @this.TryGetValue(key, out var value) ? value : default(TValue);
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> @this, TKey key)
        {
            return @this.TryGetValue(key, out var value) ? value : default(TValue);
        }
    }
}
