﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using System.Threading.Tasks;
using BlubLib.Collections.Generic;
using BlubLib.DotNetty.SimpleRmi.Reflection;
using BlubLib.Serialization;
using Sigil.NonGeneric;

namespace BlubLib.DotNetty.SimpleRmi.CodeGeneration
{
    internal static class MessageFactory<TInterface>
    {
        // ReSharper disable once StaticMemberInGenericType
        private static readonly Lazy<IReadOnlyDictionary<MethodInfo, Type>> s_requestTypes = new Lazy<IReadOnlyDictionary<MethodInfo, Type>>(BuildRequests,
            LazyThreadSafetyMode.ExecutionAndPublication);

        // ReSharper disable once StaticMemberInGenericType
        private static readonly Lazy<IReadOnlyDictionary<MethodInfo, Type>> s_responseTypes = new Lazy<IReadOnlyDictionary<MethodInfo, Type>>(BuildResponses,
            LazyThreadSafetyMode.ExecutionAndPublication);

        public static Type GetRequestType(MethodInfo methodInfo)
        {
            return s_requestTypes.Value.GetValueOrDefault(methodInfo);
        }

        public static Type GetResponseType(MethodInfo methodInfo)
        {
            return s_responseTypes.Value.GetValueOrDefault(methodInfo);
        }

        private static IReadOnlyDictionary<MethodInfo, Type> BuildRequests()
        {
            var requests = new Dictionary<MethodInfo, Type>();
            foreach (var info in InterfaceInfo<TInterface>.Methods)
                requests[info.Info] = BuildRequest(info);
            return requests;
        }

        private static IReadOnlyDictionary<MethodInfo, Type> BuildResponses()
        {
            var responses = new Dictionary<MethodInfo, Type>();
            foreach (var info in InterfaceInfo<TInterface>.Methods)
                responses[info.Info] = BuildResponse(info);
            return responses;
        }

        private static Type BuildRequest(RmiMethod method)
        {
            var typeName = $"{typeof(TInterface).FullName.Replace("+", ".")}{method.Info.Name}RequestMessage";
            var typeBuilder = TypeBuilderFactory.Create(typeName, typeof(RmiMessage));
            uint order = 0;

            foreach (var parameter in method.Parameters)
            {
                BuildProperty(typeBuilder, parameter.Info.Name, parameter.Info.ParameterType,
                    order++, parameter.SerializerType, parameter.SerializerParameters);
            }

            var type = typeBuilder.CreateTypeInfo();
            MessageFactory.Register(type);
            return type;
        }

        private static Type BuildResponse(RmiMethod method)
        {
            var returnType = method.Return.Type;
            if (typeof(Task).IsAssignableFrom(returnType))
            {
                if (returnType.GenericTypeArguments.Length == 0)
                    return null;
                returnType = returnType.GenericTypeArguments[0];
            }

            if (returnType == typeof(void))
                return null;

            var typeName = $"{typeof(TInterface).FullName.Replace("+", ".")}{method.Info.Name}ResponseMessage";
            var typeBuilder = TypeBuilderFactory.Create(typeName, typeof(RmiMessage));

            BuildProperty(typeBuilder, "ReturnValue", returnType,
                0, method.Return.SerializerType, method.Return.SerializerParameters);

            var type = typeBuilder.CreateTypeInfo();
            MessageFactory.Register(type);
            return type;
        }

        private static void BuildProperty(TypeBuilder typeBuilder, string name, Type type, uint order, Type serializerType, object[] serializerParameters)
        {
            var field = typeBuilder.DefineField(name, type, FieldAttributes.Public);
            var memberConstructor = typeof(BlubMemberAttribute)
                .GetConstructor(new[] { typeof(uint) });
            var memberAttribute = new CustomAttributeBuilder(memberConstructor,
                new object[] { order });

            field.SetCustomAttribute(memberAttribute);

            if (serializerType != null)
            {
                var serializerConstructor = typeof(BlubSerializerAttribute)
                    .GetConstructor(new[] { typeof(Type), typeof(object[]) });
                var serializerAttribute = new CustomAttributeBuilder(serializerConstructor,
                    new object[] { serializerType, serializerParameters });

                field.SetCustomAttribute(serializerAttribute);
            }
        }
    }
}
