﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using System.Threading.Tasks;
using BlubLib.DotNetty.Handlers.MessageHandling;
using BlubLib.DotNetty.SimpleRmi.Reflection;
using DotNetty.Transport.Channels;
using Sigil;
using Sigil.NonGeneric;
using Factory = System.Func<BlubLib.DotNetty.SimpleRmi.RmiService, BlubLib.DotNetty.Handlers.MessageHandling.IMessageHandler>;

namespace BlubLib.DotNetty.SimpleRmi.CodeGeneration
{
    internal static class MessageHandlerFactory<T>
        where T : RmiService
    {
        // ReSharper disable once StaticMemberInGenericType
        private static readonly Lazy<Factory> s_instance = new Lazy<Factory>(Build, LazyThreadSafetyMode.ExecutionAndPublication);

        public static IMessageHandler Get(RmiService service)
        {
            return s_instance.Value(service);
        }

        private static Factory Build()
        {
            var typeName = $"{typeof(T).FullName.Replace("+", ".")}MessageHandler";
            var typeBuilder = TypeBuilderFactory.Create(typeName, typeof(MessageHandlerBase));
            var serviceField = typeBuilder.DefineField("_rmiService", typeof(T), FieldAttributes.Private | FieldAttributes.InitOnly);

            BuildConstructor(typeBuilder, serviceField);
            foreach (var i in ServiceInfo<T>.Interfaces)
            {
                foreach (var method in i.Methods)
                {
                    if (typeof(Task).IsAssignableFrom(method.Return.Type))
                        BuildAsyncMethod(typeBuilder, serviceField, i, method);
                    else
                        BuildMethod(typeBuilder, serviceField, i, method);
                }
            }

            var handlerType = typeBuilder.CreateTypeInfo();
            var constructor = handlerType.GetConstructor(new[] { typeof(RmiService) });

            var emiter = Emit<Factory>.NewDynamicMethod();
            emiter.LoadArgument(0);
            emiter.NewObject(constructor);
            emiter.Return();

            return emiter.CreateDelegate();
        }

        private static void BuildConstructor(TypeBuilder typeBuilder, FieldInfo serviceField)
        {
            var emiter = Emit<Action<RmiService>>.BuildConstructor(typeBuilder, MethodAttributes.Public);

            emiter.LoadArgument(0);
            emiter.Call(typeof(MessageHandlerBase).GetConstructor(Type.EmptyTypes));

            emiter.LoadArgument(0);
            emiter.LoadArgument(1);
            emiter.CastClass(typeof(T));
            emiter.StoreField(serviceField);
            emiter.Return();
            emiter.CreateConstructor();
        }

        private static void BuildMethod(TypeBuilder typeBuilder, FieldInfo serviceField, InterfaceInfo interfaceInfo, RmiMethod method)
        {
            var messageFactoryType = typeof(MessageFactory<>).MakeGenericType(interfaceInfo.Type);

            var requestType = (Type)messageFactoryType
                .GetMethod(nameof(MessageFactory<object>.GetRequestType))
                .Invoke(null, new object[] { method.Info });

            var responseType = (Type)messageFactoryType
                .GetMethod(nameof(MessageFactory<object>.GetResponseType))
                .Invoke(null, new object[] { method.Info });

            var emiter = Emit.BuildInstanceMethod(typeof(void), new[] { typeof(IChannelHandlerContext), requestType },
                typeBuilder, requestType.Name, MethodAttributes.Public | MethodAttributes.HideBySig);

            SetServiceAndSession(emiter, serviceField, false);

            // this._rmiService.Method(arguments)
            emiter.LoadArgument(0);
            emiter.LoadField(serviceField);
            foreach (var parameter in method.Parameters)
            {
                emiter.LoadArgument(2);
                emiter.LoadField(requestType.GetField(parameter.Name));
            }

            emiter.CallVirtual(method.Info);

            SetServiceAndSession(emiter, serviceField, true);

            if (method.Return.Type != typeof(void))
            {
                // Send response message
                using (var returnValue = emiter.DeclareLocal(method.Return.Type))
                using (var responseMessage = emiter.DeclareLocal(responseType))
                {
                    emiter.StoreLocal(returnValue);

                    emiter.NewObject(responseMessage.LocalType);
                    emiter.StoreLocal(responseMessage);

                    // response.Guid = request.Guid
                    emiter.LoadLocal(responseMessage);
                    emiter.LoadArgument(2);
                    emiter.LoadField(typeof(RmiMessage).GetField(nameof(RmiMessage.Guid)));
                    emiter.StoreField(typeof(RmiMessage).GetField(nameof(RmiMessage.Guid)));

                    // response.ReturnValue = returnValue
                    emiter.LoadLocal(responseMessage);
                    emiter.LoadLocal(returnValue);
                    emiter.StoreField(responseMessage.LocalType.GetField("ReturnValue"));

                    emiter.LoadArgument(1);
                    emiter.LoadLocal(responseMessage);
                    emiter.Call(MessageHandlerBaseMembers.Send);
                }
            }

            emiter.Return();

            var methodBuilder = emiter.CreateMethod();
            var attributeConstructor = typeof(MessageHandlerAttribute).GetConstructor(new[] { typeof(object) });
            Debug.Assert(attributeConstructor != null, "attributeConstructor != null");

            var attributeBuilder = new CustomAttributeBuilder(attributeConstructor, new object[] { requestType });
            methodBuilder.SetCustomAttribute(attributeBuilder);
        }

        private static void BuildAsyncMethod(TypeBuilder typeBuilder, FieldBuilder serviceField, InterfaceInfo interfaceInfo, RmiMethod method)
        {
            var messageFactory = typeof(MessageFactory<>)
                .MakeGenericType(interfaceInfo.Type);

            var requestType = (Type)messageFactory
                .GetMethod(nameof(MessageFactory<object>.GetRequestType), BindingFlags.Public | BindingFlags.Static)
                .Invoke(null, new object[] { method.Info });

            var responseType = (Type)messageFactory
                .GetMethod(nameof(MessageFactory<object>.GetResponseType), BindingFlags.Public | BindingFlags.Static)
                .Invoke(null, new object[] { method.Info });

            var emiter = Emit.BuildInstanceMethod(typeof(Task), new[] { typeof(IChannelHandlerContext), requestType },
                typeBuilder, requestType.Name, MethodAttributes.Public | MethodAttributes.HideBySig);

            SetServiceAndSession(emiter, serviceField, false);

            if (method.Return.Type == typeof(Task))
            {
                // Method has no response

                // first argument for HandleAsync
                emiter.LoadArgument(0);
                emiter.LoadField(serviceField);

                // serviceField.Method(params...)
                emiter.LoadArgument(0);
                emiter.LoadField(serviceField);
                foreach (var parameter in method.Parameters)
                {
                    emiter.LoadArgument(2);
                    emiter.LoadField(requestType.GetField(parameter.Name));
                }

                emiter.CallVirtual(method.Info);
                emiter.Call(MessageHandlerBaseMembers.HandleAsync);
            }
            else
            {
                // Method has a response

                emiter.LoadArgument(0);
                emiter.LoadField(serviceField);
                emiter.LoadArgument(1);
                emiter.LoadArgument(2);

                emiter.LoadArgument(0);
                emiter.LoadField(serviceField);
                foreach (var parameter in method.Parameters)
                {
                    emiter.LoadArgument(2);
                    emiter.LoadField(requestType.GetField(parameter.Name));
                }

                emiter.CallVirtual(method.Info);

                var returnType = method.Return.Type.GenericTypeArguments[0];
                var callbackMethod = BuildCallbackMethod(typeBuilder, method.Info.Name + "Callback", returnType, responseType);

                emiter.LoadArgument(0);
                emiter.LoadFunctionPointer(callbackMethod, new[] { returnType, typeof(RmiMessage) });
                emiter.NewObject(typeof(Func<,,>).MakeGenericType(returnType, typeof(RmiMessage), typeof(RmiMessage)), typeof(object), typeof(IntPtr));
                emiter.Call(MessageHandlerBaseMembers.HandleWithResponseAsync.MakeGenericMethod(returnType));
            }

            emiter.Return();

            var methodBuilder = emiter.CreateMethod();
            var attributeConstructor = typeof(MessageHandlerAttribute).GetConstructor(new[] { typeof(object) });
            Debug.Assert(attributeConstructor != null, "attributeConstructor != null");

            var attributeBuilder = new CustomAttributeBuilder(attributeConstructor, new object[] { requestType });
            methodBuilder.SetCustomAttribute(attributeBuilder);
        }

        private static void SetServiceAndSession(Emit emiter, FieldInfo serviceField, bool setNull)
        {
            emiter.LoadArgument(0);
            emiter.LoadField(serviceField);
            if (setNull)
                emiter.LoadNull();
            else
                emiter.LoadArgument(1);

            emiter.Call(typeof(RmiService).GetProperty(nameof(RmiService.CurrentContext)).SetMethod);
        }

        private static MethodBuilder BuildCallbackMethod(TypeBuilder typeBuilder, string name, Type parameterType, Type responseType)
        {
            var emiter = Emit.BuildInstanceMethod(typeof(RmiMessage), new[] { parameterType, typeof(RmiMessage) },
                typeBuilder, name, MethodAttributes.Private | MethodAttributes.HideBySig);

            using (var message = emiter.DeclareLocal(responseType))
            {
                emiter.NewObject(message.LocalType);
                emiter.StoreLocal(message);

                emiter.LoadLocal(message);
                emiter.LoadArgument(2);
                emiter.LoadField(typeof(RmiMessage).GetField(nameof(RmiMessage.Guid)));
                emiter.StoreField(typeof(RmiMessage).GetField(nameof(RmiMessage.Guid)));

                emiter.LoadLocal(message);
                emiter.LoadArgument(1);
                emiter.StoreField(message.LocalType.GetField("ReturnValue"));

                emiter.LoadLocal(message);
                emiter.Return();
            }

            return emiter.CreateMethod();
        }
    }
}
