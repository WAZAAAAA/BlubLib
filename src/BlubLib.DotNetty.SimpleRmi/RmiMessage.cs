using System;
using BlubLib.Serialization;

namespace BlubLib.DotNetty.SimpleRmi
{
    public class RmiMessage
    {
        [BlubMember(0)]
        public Guid Guid;
    }

    [BlubContract]
    public class KeepAliveMessage : RmiMessage
    {
    }
}
