﻿using System.Collections.Generic;
using BlubLib.IO;
using BlubLib.Serialization;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;

namespace BlubLib.DotNetty.SimpleRmi.Codecs
{
    internal class MessageEncoder : MessageToMessageEncoder<RmiMessage>
    {
        private readonly BlubSerializer _serializer;

        public MessageEncoder(BlubSerializer serializer)
        {
            _serializer = serializer;
        }

        protected override void Encode(IChannelHandlerContext context, RmiMessage message, List<object> output)
        {
            var buffer = context.Allocator.Buffer(16);
            using (var w = new WriteOnlyByteBufferStream(buffer, false).ToBinaryWriter(false))
            {
                var opCode = MessageFactory.GetOpCode(message.GetType());
                w.Write(opCode);
                _serializer.Serialize(w, (object)message);
            }

            output.Add(buffer);
        }
    }
}
