﻿using System.Collections.Generic;
using BlubLib.IO;
using BlubLib.Serialization;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;

namespace BlubLib.DotNetty.SimpleRmi.Codecs
{
    internal class MessageDecoder : MessageToMessageDecoder<IByteBuffer>
    {
        private readonly BlubSerializer _serializer;

        public MessageDecoder(BlubSerializer serializer)
        {
            _serializer = serializer;
        }

        protected override void Decode(IChannelHandlerContext context, IByteBuffer buffer, List<object> output)
        {
            using (var r = new ReadOnlyByteBufferStream(buffer, false).ToBinaryReader(false))
            {
                var opCode = r.ReadString();
                var message = MessageFactory.GetMessage(_serializer, opCode, r);
                output.Add(message);
            }
        }
    }
}
