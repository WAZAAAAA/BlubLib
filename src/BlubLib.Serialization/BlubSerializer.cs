﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using BlubLib.Collections.Concurrent;
using BlubLib.Collections.Generic;
using BlubLib.IO;
using BlubLib.Reflection;
using BlubLib.Serialization.Serializers;
using Sigil;

namespace BlubLib.Serialization
{
    using DeserializeWithReader = Func<BlubSerializer, BinaryReader, object>;
    using DeserializeWithStream = Func<BlubSerializer, Stream, object>;
    using SerializeWithStream = Action<BlubSerializer, Stream, object>;
    using SerializeWithWriter = Action<BlubSerializer, BinaryWriter, object>;

    /// <summary>
    /// Simple and fast serializer
    /// </summary>
    public class BlubSerializer
    {
        /// <summary>
        /// Gets a singleton instance
        /// </summary>
        public static BlubSerializer Instance { get; } = new BlubSerializer();

        private static readonly MethodInfo s_serializeWithWriterMethod;
        private static readonly MethodInfo s_serializeWithStreamMethod;
        private static readonly MethodInfo s_deserializeWithReaderMethod;
        private static readonly MethodInfo s_deserializeWithStreamMethod;

        private readonly TypeModel _typeModel;
        private readonly IList<ISerializer> _serializers;
        private readonly IList<ISerializerCompiler> _compilers;
        private readonly IReadOnlyDictionary<Type, ISerializerCompiler> _primitiveCompilers;
        private readonly ConcurrentDictionary<Type, SerializeWithWriter> _serializeWithWriterCache;
        private readonly ConcurrentDictionary<Type, SerializeWithStream> _serializeWithStreamCache;
        private readonly ConcurrentDictionary<Type, DeserializeWithReader> _deserializeWithReaderCache;
        private readonly ConcurrentDictionary<Type, DeserializeWithStream> _deserializeWithStreamCache;

        static BlubSerializer()
        {
            s_serializeWithWriterMethod = ReflectionHelper.GetMethod((BlubSerializer _) =>
                    _.Serialize<object>(default(BinaryWriter), default(object)))
                .GetGenericMethodDefinition();

            s_serializeWithStreamMethod = ReflectionHelper.GetMethod((BlubSerializer _) =>
                    _.Serialize<object>(default(Stream), default(object)))
                .GetGenericMethodDefinition();

            s_deserializeWithReaderMethod = ReflectionHelper.GetMethod((BlubSerializer _) =>
                    _.Deserialize<object>(default(BinaryReader)))
                .GetGenericMethodDefinition();

            s_deserializeWithStreamMethod = ReflectionHelper.GetMethod((BlubSerializer _) =>
                    _.Deserialize<object>(default(Stream)))
                .GetGenericMethodDefinition();
        }

        public BlubSerializer()
        {
            _typeModel = new TypeModel(this);
            _serializers = new List<ISerializer>();
            _compilers = new List<ISerializerCompiler>();
            _serializeWithWriterCache = new ConcurrentDictionary<Type, SerializeWithWriter>();
            _serializeWithStreamCache = new ConcurrentDictionary<Type, SerializeWithStream>();
            _deserializeWithReaderCache = new ConcurrentDictionary<Type, DeserializeWithReader>();
            _deserializeWithStreamCache = new ConcurrentDictionary<Type, DeserializeWithStream>();
            _primitiveCompilers = new Dictionary<Type, ISerializerCompiler>
            {
                { typeof(bool), new PrimitiveSerializer(typeof(bool)) },
                { typeof(byte), new PrimitiveSerializer(typeof(byte)) },
                { typeof(char), new PrimitiveSerializer(typeof(char)) },
                { typeof(decimal), new PrimitiveSerializer(typeof(decimal)) },
                { typeof(double), new PrimitiveSerializer(typeof(double)) },
                { typeof(float), new PrimitiveSerializer(typeof(float)) },
                { typeof(short), new PrimitiveSerializer(typeof(short)) },
                { typeof(int), new PrimitiveSerializer(typeof(int)) },
                { typeof(long), new PrimitiveSerializer(typeof(long)) },
                { typeof(sbyte), new PrimitiveSerializer(typeof(sbyte)) },
                { typeof(string), new PrimitiveSerializer(typeof(string)) },
                { typeof(ushort), new PrimitiveSerializer(typeof(ushort)) },
                { typeof(uint), new PrimitiveSerializer(typeof(uint)) },
                { typeof(ulong), new PrimitiveSerializer(typeof(ulong)) },
                { typeof(Guid), new GuidSerializer() }
            };

            AddSerializer(new EnumSerializer());
        }

        /// <summary>
        /// Compiles all serializers for types marked with <see cref="BlubContractAttribute"/>
        /// </summary>
        public void Compile(params Assembly[] assemblies)
        {
            assemblies = assemblies ?? AppDomain.CurrentDomain.GetAssemblies();
            foreach(var assembly in assemblies)
            foreach (var type in assembly.DefinedTypes.Where(x => x.GetCustomAttribute<BlubContractAttribute>() != null))
                GetOrCreateSerializer(type);
        }

        /// <summary>
        /// Registers a <see cref="ISerializerCompiler"/> for serialization
        /// </summary>
        /// <param name="compiler">The serializer compiler to register</param>
        public void AddSerializer(ISerializerCompiler compiler)
        {
            if (_compilers.Any(x => x.GetType() == compiler.GetType()))
                throw new ArgumentException("Serializer was already added", nameof(compiler));

            _compilers.Add(compiler);
        }

        /// <summary>
        /// Registers a <see cref="ISerializer{T}"/> for serialization
        /// </summary>
        /// <typeparam name="T">The object or value type</typeparam>
        /// <param name="serializer">The serializer to register</param>
        public void AddSerializer<T>(ISerializer<T> serializer)
        {
            if (_serializers.Any(x => x.GetType() == serializer.GetType()))
                throw new ArgumentException("Serializer was already added", nameof(serializer));

            _serializers.Add(serializer);
        }

        /// <summary>
        /// Gets a <see cref="ISerializer{T}"/> instance for the given type
        /// </summary>
        /// <typeparam name="T">The object or value type</typeparam>
        /// <returns>A <see cref="ISerializer{T}"/> instance which can be used for serialization outside of <see cref="BlubSerializer"/></returns>
        public ISerializer<T> GetSerializer<T>()
        {
            var type = typeof(T);
            var serializer = GetOrCreateSerializer(type);
            if (serializer == null)
                throw new ArgumentException($"{type.FullName} has no properties to serialize");

            return (ISerializer<T>)serializer;
        }

        /// <summary>
        /// Serializes an object
        /// </summary>
        /// <param name="writer">The writer to serialize the object to</param>
        /// <param name="value">The object to serialize</param>
        public void Serialize(BinaryWriter writer, object value)
        {
            var type = value.GetType();
            var func = _serializeWithWriterCache.GetValueOrDefault(type);
            if (func != null)
            {
                func(this, writer, value);
                return;
            }

            lock (_serializeWithWriterCache)
            {
                func = _serializeWithWriterCache.GetValueOrDefault(type);
                if (func != null)
                {
                    func(this, writer, value);
                    return;
                }

                var emiter = Emit<SerializeWithWriter>.NewDynamicMethod();
                var method = s_serializeWithWriterMethod.MakeGenericMethod(type);
                EmitSerialize(emiter, type, method);

                func = emiter.CreateDelegate();
                _serializeWithWriterCache.TryAdd(type, func);
            }

            func(this, writer, value);
        }

        /// <summary>
        /// Serializes an object
        /// </summary>
        /// <param name="stream">The stream to serialize the object to</param>
        /// <param name="value">The object to serialize</param>
        public void Serialize(Stream stream, object value)
        {
            var type = value.GetType();
            var func = _serializeWithStreamCache.GetValueOrDefault(type);
            if (func != null)
            {
                func(this, stream, value);
                return;
            }

            lock (_serializeWithStreamCache)
            {
                func = _serializeWithStreamCache.GetValueOrDefault(type);
                if (func != null)
                {
                    func(this, stream, value);
                    return;
                }

                var emiter = Emit<SerializeWithStream>.NewDynamicMethod();
                var method = s_serializeWithStreamMethod.MakeGenericMethod(type);
                EmitSerialize(emiter, type, method);

                func = emiter.CreateDelegate();
                _serializeWithStreamCache.TryAdd(type, func);
            }

            func(this, stream, value);
        }

        /// <summary>
        /// Serializes an object
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="writer">The writer to serialize the object to</param>
        /// <param name="value">The object to serialize</param>
        public void Serialize<T>(BinaryWriter writer, T value)
        {
            GetSerializer<T>().Serialize(this, writer, value);
        }

        /// <summary>
        /// Serializes an object
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="stream">The stream to serialize the object to</param>
        /// <param name="value">The object to serialize</param>
        public void Serialize<T>(Stream stream, T value)
        {
            using (var writer = stream.ToBinaryWriter(true))
                GetSerializer<T>().Serialize(this, writer, value);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <param name="reader">The reader to deserialize the object from</param>
        /// <param name="type">The object type to deserialize</param>
        /// <returns>The deserialized object</returns>
        public object Deserialize(BinaryReader reader, Type type)
        {
            var func = _deserializeWithReaderCache.GetValueOrDefault(type);
            if (func != null)
                return func(this, reader);

            lock (_deserializeWithReaderCache)
            {
                func = _deserializeWithReaderCache.GetValueOrDefault(type);
                if (func != null)
                    return func(this, reader);

                var emiter = Emit<DeserializeWithReader>.NewDynamicMethod();
                var method = s_deserializeWithReaderMethod.MakeGenericMethod(type);
                EmitDeserialize(emiter, type, method);

                func = emiter.CreateDelegate();
                _deserializeWithReaderCache.TryAdd(type, func);
            }

            return func(this, reader);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <param name="stream">The stream to deserialize the object from</param>
        /// <param name="type">The object type to deserialize</param>
        /// <returns>The deserialized object</returns>
        public object Deserialize(Stream stream, Type type)
        {
            var func = _deserializeWithStreamCache.GetValueOrDefault(type);
            if (func != null)
                return func(this, stream);

            lock (_deserializeWithStreamCache)
            {
                func = _deserializeWithStreamCache.GetValueOrDefault(type);
                if (func != null)
                    return func(this, stream);

                var emiter = Emit<DeserializeWithStream>.NewDynamicMethod();
                var method = s_deserializeWithStreamMethod.MakeGenericMethod(type);
                EmitDeserialize(emiter, type, method);

                func = emiter.CreateDelegate();
                _deserializeWithStreamCache.TryAdd(type, func);
            }

            return func(this, stream);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <typeparam name="T">The object type to deserialize</typeparam>
        /// <param name="reader">The reader to deserialize the object from</param>
        /// <returns>The deserialized object</returns>
        public T Deserialize<T>(BinaryReader reader)
        {
            return GetSerializer<T>().Deserialize(this, reader);
        }

        /// <summary>
        /// Deserializes an object
        /// </summary>
        /// <typeparam name="T">The object type to deserialize</typeparam>
        /// <param name="stream">The stream to deserialize the object from</param>
        /// <returns>The deserialized object</returns>
        public T Deserialize<T>(Stream stream)
        {
            using (var reader = stream.ToBinaryReader(true))
                return GetSerializer<T>().Deserialize(this, reader);
        }

        internal ISerializer GetOrCreateSerializer(Type type)
        {
            // ReSharper disable once InconsistentlySynchronizedField
            var descriptor = _typeModel.GetDescriptor(type);
            if (descriptor?.Serializer != null)
                return descriptor.Serializer;

            lock (_serializers)
            {
                descriptor = _typeModel.GetOrCreateDescriptor(type);

                if (descriptor == null)
                    return null;

                if (descriptor.Serializer != null)
                    return descriptor.Serializer;

                var generator = new SerializerGenerator(this, descriptor);
                var serializer = generator.Generate();
                if (serializer == null)
                    return null;

                descriptor.Serializer = serializer;
                return serializer;
            }
        }

        internal ISerializerCompiler GetCompilerForType(Type type)
        {
            return _compilers.FirstOrDefault(compiler => compiler.CanHandle(type)) ?? _primitiveCompilers.GetValueOrDefault(type);
        }

        internal ISerializer GetSerializerForType(Type type)
        {
            return _serializers.FirstOrDefault(serializer => serializer.CanHandle(type));
        }

        private static void EmitSerialize(Emit<SerializeWithWriter> emiter, Type type, MethodInfo method)
        {
            // serializer.Serialize(BlubSerializer, BinaryWriter, T value)

            emiter.LoadArgument(0);
            emiter.LoadArgument(1);
            emiter.LoadArgument(2);
            if (type.IsValueType)
                emiter.UnboxAny(type);
            else
                emiter.CastClass(type);

            emiter.Call(method);
            emiter.Return();
        }

        private static void EmitSerialize(Emit<SerializeWithStream> emiter, Type type, MethodInfo method)
        {
            // serializer.Serialize(BlubSerializer, Stream, T value)

            emiter.LoadArgument(0);
            emiter.LoadArgument(1);
            emiter.LoadArgument(2);
            if (type.IsValueType)
                emiter.UnboxAny(type);
            else
                emiter.CastClass(type);

            emiter.Call(method);
            emiter.Return();
        }

        private static void EmitDeserialize(Emit<DeserializeWithReader> emiter, Type type, MethodInfo method)
        {
            // serializer.Deserialize(BlubSerializer, BinaryReader)

            emiter.LoadArgument(0);
            emiter.LoadArgument(1);
            emiter.Call(method);
            if (type.IsValueType)
                emiter.Box(type);
            else
                emiter.CastClass<object>();

            emiter.Return();
        }

        private static void EmitDeserialize(Emit<DeserializeWithStream> emiter, Type type, MethodInfo method)
        {
            // serializer.Deserialize(BlubSerializer, Stream)

            emiter.LoadArgument(0);
            emiter.LoadArgument(1);
            emiter.Call(method);
            if (type.IsValueType)
                emiter.Box(type);
            else
                emiter.CastClass<object>();

            emiter.Return();
        }
    }
}
