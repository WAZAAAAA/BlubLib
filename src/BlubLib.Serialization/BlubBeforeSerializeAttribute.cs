﻿using System;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Executes the marked method before the serialization of a member
    /// </summary>
    /// <remarks>
    /// Method signature has to be <code>bool (BlubSerializer, BinaryWriter, string memberName)</code>
    /// The serialization for the member gets skipped when the method returns true
    /// Other methods marked with <see cref="BlubBeforeSerializeAttribute"/> get skipped too if true is returned
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method,
        Inherited = false, AllowMultiple = false)]
    public class BlubBeforeSerializeAttribute : Attribute
    {
    }
}
