﻿using System;
using Sigil;

namespace BlubLib.Serialization.Serializers
{
    /// <summary>
    /// Serializer for enum types
    /// </summary>
    public class EnumSerializer : ISerializerCompiler
    {
        private readonly Type _serializeAsType;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumSerializer"/> class
        /// The serializer will serialize the enum with the derived base type
        /// </summary>
        public EnumSerializer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumSerializer"/> class
        /// The serializer will serialize the enum with the given type
        /// </summary>
        /// <param name="serializeAsType">A non-character primitive type. Supported types are int8, uint8, int16, uint16, int32, uint32, int64, uint64</param>
        /// <remarks>The serializer will convert the enum to the given type using the conv.i* IL instrcution</remarks>
        public EnumSerializer(Type serializeAsType)
        {
            switch (serializeAsType.GetTypeCode())
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    break;

                default:
                    throw new ArgumentException("Supported types are int8, uint8, int16, uint16, int32, uint32, int64, uint64");
            }

            _serializeAsType = serializeAsType;
        }

        /// <inheritdoc/>
        public bool CanHandle(Type type)
        {
            return type.IsEnum;
        }

        /// <inheritdoc/>
        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            var underlyingType = value.LocalType.GetEnumUnderlyingType();
            var typeToUse = _serializeAsType ?? underlyingType;

            using (var tmp = ctx.Emit.DeclareLocal(typeToUse))
            {
                ctx.EmitDeserialize(tmp);
                ctx.Emit.LoadLocal(tmp);
                if (underlyingType != typeToUse)
                    ctx.Emit.Convert(underlyingType);

                ctx.Emit.StoreLocal(value);
            }
        }

        /// <inheritdoc/>
        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            var underlyingType = value.LocalType.GetEnumUnderlyingType();
            var typeToUse = _serializeAsType ?? underlyingType;

            using (var tmp = ctx.Emit.DeclareLocal(typeToUse))
            {
                ctx.Emit.LoadLocal(value);
                if (underlyingType != typeToUse)
                    ctx.Emit.Convert(underlyingType);

                ctx.Emit.StoreLocal(tmp);
                ctx.EmitSerialize(tmp);
            }
        }
    }
}
