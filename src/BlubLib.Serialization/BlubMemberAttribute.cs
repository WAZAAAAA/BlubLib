﻿using System;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Tells the <see cref="BlubSerializer"/> to include this member and optionally the member order
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class BlubMemberAttribute : Attribute
    {
        public uint? Order { get; set; }

        public BlubMemberAttribute()
        {
        }

        public BlubMemberAttribute(uint order)
        {
            Order = order;
        }
    }
}
