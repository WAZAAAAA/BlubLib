﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using BlubLib.Collections.Concurrent;

namespace BlubLib.Serialization
{
    internal class TypeModel
    {
        private readonly BlubSerializer _serializer;
        private readonly ConcurrentDictionary<Type, Descriptor> _descriptors = new ConcurrentDictionary<Type, Descriptor>();

        public TypeModel(BlubSerializer serializer)
        {
            _serializer = serializer;
        }

        public Descriptor GetDescriptor(Type type)
        {
            return _descriptors.GetValueOrDefault(type);
        }

        public Descriptor GetOrCreateDescriptor(Type type)
        {
            var descriptor = GetDescriptor(type) ?? CreateDescriptor(type);
            _descriptors.TryAdd(type, descriptor);
            return descriptor;
        }

        private Descriptor CreateDescriptor(Type type)
        {
            Descriptor parent = null;

            // Ignore Object, ValueType and Enum parent types
            if (type.BaseType != typeof(object) && type.BaseType != typeof(ValueType) && type.BaseType != typeof(Enum))
                parent = GetOrCreateDescriptor(type.BaseType);

            var contractAttribute = type.GetCustomAttribute<BlubContractAttribute>();
            var serializerAttribute = type.GetCustomAttribute<BlubSerializerAttribute>();
            var descriptor = new Descriptor { Type = type, Parent = parent };

            if (serializerAttribute != null)
            {
                // Serializer was provided by the user

                if (typeof(ISerializer).IsAssignableFrom(serializerAttribute.SerializerType))
                {
                    descriptor.Serializer = (ISerializer)Activator.CreateInstance(serializerAttribute.SerializerType,
                        serializerAttribute.SerializerParameters);
                    return descriptor;
                }

                if (typeof(ISerializerCompiler).IsAssignableFrom(serializerAttribute.SerializerType))
                {
                    descriptor.Compiler = (ISerializerCompiler)Activator.CreateInstance(serializerAttribute.SerializerType,
                        serializerAttribute.SerializerParameters);
                    return descriptor;
                }

                throw new Exception($"Invalid serializer assigned to {type.FullName}");
            }

            // Check if there is a serializer available
            descriptor.Serializer = _serializer.GetSerializerForType(type);
            if (descriptor.Serializer == null)
                descriptor.Compiler = _serializer.GetCompilerForType(type);

            // We already have a serializer - No need to check members
            if (descriptor.Serializer != null || descriptor.Compiler != null)
                return descriptor;

            // No serializer was provided
            // Go through all members and try to find the needed serializers for each member

            var typeInfo = type.GetTypeInfo();
            descriptor.Members = new SortedList<uint, MemberDescriptor>();
            descriptor.BeforeSerializeMethods = new List<MethodInfo>();
            descriptor.AfterSerializeMethods = new List<MethodInfo>();
            descriptor.BeforeDeserializeMethods = new List<MethodInfo>();
            descriptor.AfterDeserializeMethods = new List<MethodInfo>();
            var orders = new HashSet<uint>();

            var members = typeInfo.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
            foreach (var member in members)
            {
                var propertyInfo = member as PropertyInfo;
                var fieldInfo = member as FieldInfo;
                var methodInfo = member as MethodInfo;

                // We only look for properties, fields and methods
                if (propertyInfo == null && fieldInfo == null && methodInfo == null)
                    continue;

                if (member.GetCustomAttribute<CompilerGeneratedAttribute>() != null)
                    continue;

                // Register before/after serialization hooks
                if (methodInfo != null)
                {
                    var parameterTypes = methodInfo.GetParameters().Select(x => x.ParameterType).ToArray();
                    var beforeSerializeAttribute = member.GetCustomAttribute<BlubBeforeSerializeAttribute>();
                    if (beforeSerializeAttribute != null)
                    {
                        if (methodInfo.ReturnType != typeof(bool) ||
                            !parameterTypes.SequenceEqual(new[] { typeof(BlubSerializer), typeof(BinaryWriter), typeof(string) }))
                        {
                            throw new Exception($"Methods marked with {nameof(BlubBeforeSerializeAttribute)} need the signature bool ({nameof(BlubSerializer)}, BinaryWriter, string)");
                        }

                        descriptor.BeforeSerializeMethods.Add(methodInfo);
                    }

                    var afterSerializeAttribute = member.GetCustomAttribute<BlubAfterSerializeAttribute>();
                    if (afterSerializeAttribute != null)
                    {
                        if (methodInfo.ReturnType != typeof(void) ||
                            !parameterTypes.SequenceEqual(new[] { typeof(BlubSerializer), typeof(BinaryWriter), typeof(string) }))
                        {
                            throw new Exception($"Methods marked with {nameof(BlubAfterSerializeAttribute)} need the signature void ({nameof(BlubSerializer)}, BinaryWriter, string)");
                        }

                        descriptor.AfterSerializeMethods.Add(methodInfo);
                    }

                    var beforeDeserializeAttribute = member.GetCustomAttribute<BlubBeforeDeserializeAttribute>();
                    if (beforeDeserializeAttribute != null)
                    {
                        if (methodInfo.ReturnType != typeof(bool) ||
                            !parameterTypes.SequenceEqual(new[] { typeof(BlubSerializer), typeof(BinaryReader), typeof(string) }))
                        {
                            throw new Exception($"Methods marked with {nameof(BlubBeforeDeserializeAttribute)} need the signature bool ({nameof(BlubSerializer)}, BinaryReader, string)");
                        }

                        descriptor.BeforeDeserializeMethods.Add(methodInfo);
                    }

                    var afterDeserializeAttribute = member.GetCustomAttribute<BlubAfterDeserializeAttribute>();
                    if (afterDeserializeAttribute != null)
                    {
                        if (methodInfo.ReturnType != typeof(void) ||
                            !parameterTypes.SequenceEqual(new[] { typeof(BlubSerializer), typeof(BinaryReader), typeof(string) }))
                        {
                            throw new Exception($"Methods marked with {nameof(BlubAfterDeserializeAttribute)} need the signature void ({nameof(BlubSerializer)}, BinaryReader, string)");
                        }

                        descriptor.AfterDeserializeMethods.Add(methodInfo);
                    }

                    continue;
                }

                var ignoreAttribute = member.GetCustomAttribute<BlubIgnoreAttribute>();
                if (ignoreAttribute != null)
                    continue;

                var memberAttribute = member.GetCustomAttribute<BlubMemberAttribute>();

                // If type is marked with BlubContractAttribute only include members with BlubMemberAttribute
                if (contractAttribute != null && memberAttribute == null)
                    continue;

                if (propertyInfo?.PropertyType == member.DeclaringType || fieldInfo?.FieldType == member.DeclaringType)
                    throw new Exception("The declaring type cant be used as a member");

                if (propertyInfo != null && (!propertyInfo.CanWrite || !propertyInfo.CanRead))
                    throw new Exception($"Property({member.DeclaringType.FullName}.{member.Name}) needs a getter and setter");

                if (memberAttribute?.Order == null && orders.Count > 0)
                    throw new Exception("Member order has to be provided on every member if used");

                if (memberAttribute?.Order != null && !orders.Add(memberAttribute.Order.Value))
                    throw new Exception("Member order has to be unique");

                descriptor.Members.Add(memberAttribute?.Order ?? (uint)descriptor.Members.Count, CreateDescriptorFromMember(member));
            }

            return descriptor;
        }

        private MemberDescriptor CreateDescriptorFromMember(MemberInfo member)
        {
            MemberDescriptor descriptor = null;
            Type membertype = null;
            switch (member)
            {
                case PropertyInfo propertyInfo:
                    membertype = propertyInfo.PropertyType;
                    descriptor = new PropertyDescriptor
                    {
                        Name = member.Name,
                        Type = membertype,
                        PropertyInfo = propertyInfo
                    };
                    break;

                case FieldInfo fieldInfo:
                    membertype = fieldInfo.FieldType;
                    descriptor = new FieldDescriptor
                    {
                        Name = member.Name,
                        Type = membertype,
                        FieldInfo = fieldInfo
                    };
                    break;

                default:
                    throw new ArgumentException("Only property or field members are supported", nameof(member));
            }

            var serializerAttribute = member.GetCustomAttribute<BlubSerializerAttribute>();
            if (serializerAttribute != null)
            {
                // Serializer was provided by the user

                if (typeof(ISerializer).IsAssignableFrom(serializerAttribute.SerializerType))
                {
                    descriptor.Serializer = (ISerializer)Activator.CreateInstance(serializerAttribute.SerializerType,
                        serializerAttribute.SerializerParameters);
                }
                else if (typeof(ISerializerCompiler).IsAssignableFrom(serializerAttribute.SerializerType))
                {
                    descriptor.Compiler = (ISerializerCompiler)Activator.CreateInstance(serializerAttribute.SerializerType,
                        serializerAttribute.SerializerParameters);
                }
                else
                {
                    throw new Exception($"Invalid serializer assigned to {member.DeclaringType.FullName}.{member.Name}");
                }
            }
            else
            {
                // No serializer was provided
                // Check if there are serializers registered

                descriptor.Compiler = _serializer.GetCompilerForType(membertype);
                if (descriptor.Compiler == null)
                {
                    descriptor.Serializer = _serializer.GetOrCreateSerializer(membertype);
                    if (descriptor.Serializer == null)
                        throw new Exception($"No serializer available for {member.DeclaringType.FullName}.{member.Name}");
                }
            }

            return descriptor;
        }
    }

    internal class Descriptor
    {
        private readonly object _mutex = new object();
        private Queue<Descriptor> _stack;

        public Type Type { get; set; }
        public Descriptor Parent { get; set; }
        public SortedList<uint, MemberDescriptor> Members { get; set; }

        public IList<MethodInfo> BeforeSerializeMethods { get; set; }
        public IList<MethodInfo> AfterSerializeMethods { get; set; }
        public IList<MethodInfo> BeforeDeserializeMethods { get; set; }
        public IList<MethodInfo> AfterDeserializeMethods { get; set; }

        public ISerializer Serializer { get; set; }
        public ISerializerCompiler Compiler { get; set; }

        /// <summary>
        /// Provides the whole type hierarchy from base to upper
        /// </summary>
        /// <returns>Enumerable type hierarchy</returns>
        public IEnumerable<Descriptor> GetTree()
        {
            lock (_mutex)
            {
                if (_stack == null)
                {
                    _stack = new Queue<Descriptor>();
                    AddRecursive(this);
                }

                return _stack;
            }
        }

        private void AddRecursive(Descriptor descriptor)
        {
            if (descriptor.Parent != null)
                AddRecursive(descriptor.Parent);

            _stack.Enqueue(descriptor);
        }
    }

    internal class MemberDescriptor
    {
        public string Name { get; set; }
        public Type Type { get; set; }
        public ISerializer Serializer { get; set; }
        public ISerializerCompiler Compiler { get; set; }
    }

    internal class PropertyDescriptor : MemberDescriptor
    {
        public PropertyInfo PropertyInfo { get; set; }
    }

    internal class FieldDescriptor : MemberDescriptor
    {
        public FieldInfo FieldInfo { get; set; }
    }
}
