using System;
using Sigil;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Provides methods to emit IL code for a serializer
    /// </summary>
    public interface ISerializerCompiler
    {
        /// <summary>
        /// Gets an indication whether the serializer can handle the type
        /// </summary>
        /// <param name="type">The type to handle</param>
        /// <returns>True if the serializer can handle the type</returns>
        bool CanHandle(Type type);

        /// <summary>
        /// Emits IL code for serialization
        /// </summary>
        /// <param name="ctx">Current context containing the <see cref="Sigil.NonGeneric.Emit"/> and <see cref="BlubSerializer"/> instance</param>
        /// <param name="value">The value to serialize</param>
        void EmitSerialize(CompilerContext ctx, Local value);

        /// <summary>
        /// Emits IL code for deserialization
        /// </summary>
        /// <param name="ctx">Current context containing the <see cref="Sigil.NonGeneric.Emit"/> and <see cref="BlubSerializer"/> instance</param>
        /// <param name="value">The value to serialize</param>
        void EmitDeserialize(CompilerContext ctx, Local value);
    }
}
