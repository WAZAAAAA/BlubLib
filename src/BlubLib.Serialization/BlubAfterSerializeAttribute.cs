﻿using System;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Executes the marked method after the serialization of a member
    /// </summary>
    /// <remarks>
    /// Method signature has to be <code>void MethodName(BlubSerializer serializer, BinaryWriter writer, string memberName)</code>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method,
        Inherited = false, AllowMultiple = false)]
    public class BlubAfterSerializeAttribute : Attribute
    {
    }
}
