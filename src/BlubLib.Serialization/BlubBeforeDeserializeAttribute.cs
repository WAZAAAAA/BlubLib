﻿using System;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Executes the marked method before the deserialization of a member
    /// </summary>
    /// <remarks>
    /// Method signature has to be <code>bool MethodName(BlubSerializer serializer, BinaryReader reader, string memberName)</code>
    /// The deserialization for the member gets skipped when the method returns true
    /// Other methods marked with <see cref="BlubBeforeDeserializeAttribute"/> get skipped too if true is returned
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method,
        Inherited = false, AllowMultiple = false)]
    public class BlubBeforeDeserializeAttribute : Attribute
    {
    }
}
