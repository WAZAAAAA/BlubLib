﻿using System.Threading;
using System.Threading.Tasks;
using BlubLib.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BlubLib.Tests.Threading.Tasks
{
    [TestClass]
    public class TaskExtensionsTests
    {
        [TestMethod]
        public void WaitAsync_WithResult()
        {
            var expected = 123;
            var tcs = new TaskCompletionSource<int>();
            tcs.TrySetResult(expected);
            var result = tcs.Task.WaitAsync(CancellationToken.None).WaitEx();
            Assert.AreEqual(result, expected);

            tcs = new TaskCompletionSource<int>();
            Assert.ThrowsException<TaskCanceledException>(() =>
                tcs.Task.WaitAsync(new CancellationTokenSource(500).Token).WaitEx());
        }
    }
}
