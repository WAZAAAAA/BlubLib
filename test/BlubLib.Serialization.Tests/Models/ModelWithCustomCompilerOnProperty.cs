﻿using BlubLib.Serialization.Tests.Serializers;

namespace BlubLib.Serialization.Tests.Models
{
    [BlubContract]
    public class ModelWithCustomCompilerOnProperty
    {
        [BlubMember(0)]
        [BlubSerializer(typeof(SimpleModelCompiler))]
        public SimpleModel SimpleModel { get; set; }
    }
}
