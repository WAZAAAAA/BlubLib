﻿using System;
using System.IO;

namespace BlubLib.Serialization.Tests.Models
{
    public class HooksModel
    {
        public byte A { get; set; }
        public byte B { get; set; }

        public static Func<BlubSerializer, BinaryWriter, string, bool> BeforeSerializeFunc { get; set; }
        public static Func<BlubSerializer, BinaryReader, string, bool> BeforeDeserializeFunc { get; set; }
        public static Action<BlubSerializer, BinaryWriter, string> AfterSerializeFunc { get; set; }
        public static Action<BlubSerializer, BinaryReader, string> AfterDeserializeFunc { get; set; }

        [BlubBeforeSerialize]
        public bool BeforeSerialize(BlubSerializer serializer, BinaryWriter writer, string memberName)
        {
            return BeforeSerializeFunc != null && BeforeSerializeFunc(serializer, writer, memberName);
        }

        [BlubBeforeDeserialize]
        public bool BeforeDeserialize(BlubSerializer serializer, BinaryReader reader, string memberName)
        {
            return BeforeDeserializeFunc != null && BeforeDeserializeFunc(serializer, reader, memberName);
        }

        [BlubAfterSerialize]
        public void AfterSerialize(BlubSerializer serializer, BinaryWriter writer, string memberName)
        {
            AfterSerializeFunc?.Invoke(serializer, writer, memberName);
        }

        [BlubAfterDeserialize]
        public void AfterDeserialize(BlubSerializer serializer, BinaryReader reader, string memberName)
        {
            AfterDeserializeFunc?.Invoke(serializer, reader, memberName);
        }
    }
}
