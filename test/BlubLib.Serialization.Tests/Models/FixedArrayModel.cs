﻿using BlubLib.Serialization.Serializers;

namespace BlubLib.Serialization.Tests.Models
{
    [BlubContract]
    public class FixedArrayModel
    {
        [BlubMember(0)]
        [BlubSerializer(typeof(FixedArraySerializer), 3)]
        public byte[] Value { get; set; }

        [BlubMember(1)]
        [BlubSerializer(typeof(FixedArraySerializer), 2)]
        public int[] Value2 { get; set; }

        public FixedArrayModel()
        {
        }

        public FixedArrayModel(byte[] value, int[] value2)
        {
            Value = value;
            Value2 = value2;
        }
    }
}
