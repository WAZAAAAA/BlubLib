﻿namespace BlubLib.Serialization.Tests.Models
{
    public class MemberOrderNotUniqueModel
    {
        [BlubMember(1)]
        public byte A { get; set; }

        [BlubMember(1)]
        public byte B { get; set; }
    }
}
