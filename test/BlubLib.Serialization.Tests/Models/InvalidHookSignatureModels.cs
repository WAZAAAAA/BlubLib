﻿using System.IO;

namespace BlubLib.Serialization.Tests.Models
{
    public static class InvalidBeforeSerializeModel
    {
        public class InvalidParameter1Model
        {
            [BlubBeforeSerialize]
            public bool Hook(string p1, BinaryWriter p2, string p3)
            {
                return false;
            }
        }

        public class InvalidParameter2Model
        {
            [BlubBeforeSerialize]
            public bool Hook(BlubSerializer p1, string p2, string p3)
            {
                return false;
            }
        }

        public class InvalidParameter3Model
        {
            [BlubBeforeSerialize]
            public bool Hook(BlubSerializer p1, BinaryWriter p2, int p3)
            {
                return false;
            }
        }

        public class InvalidParameter4Model
        {
            [BlubBeforeSerialize]
            public bool Hook(BlubSerializer p1)
            {
                return false;
            }
        }

        public class InvalidReturnModel
        {
            [BlubBeforeSerialize]
            public void Hook(BlubSerializer p1, BinaryWriter p2, string p3)
            {
            }
        }
    }

    public static class InvalidBeforeDeserializeModel
    {
        public class InvalidParameter1Model
        {
            [BlubBeforeDeserialize]
            public bool Hook(string p1, BinaryReader p2, string p3)
            {
                return false;
            }
        }

        public class InvalidParameter2Model
        {
            [BlubBeforeDeserialize]
            public bool Hook(BlubSerializer p1, string p2, string p3)
            {
                return false;
            }
        }

        public class InvalidParameter3Model
        {
            [BlubBeforeDeserialize]
            public bool Hook(BlubSerializer p1, BinaryReader p2, int p3)
            {
                return false;
            }
        }

        public class InvalidParameter4Model
        {
            [BlubBeforeDeserialize]
            public bool Hook(BlubSerializer p1)
            {
                return false;
            }
        }

        public class InvalidReturnModel
        {
            [BlubBeforeDeserialize]
            public void Hook(BlubSerializer p1, BinaryReader p2, string p3)
            {
            }
        }
    }

    public static class InvalidAfterSerializeModel
    {
        public class InvalidParameter1Model
        {
            [BlubAfterSerialize]
            public void Hook(string p1, BinaryWriter p2, string p3)
            {
            }
        }

        public class InvalidParameter2Model
        {
            [BlubAfterSerialize]
            public void Hook(BlubSerializer p1, string p2, string p3)
            {
            }
        }

        public class InvalidParameter3Model
        {
            [BlubAfterSerialize]
            public void Hook(BlubSerializer p1, BinaryWriter p2, int p3)
            {
            }
        }

        public class InvalidParameter4Model
        {
            [BlubAfterSerialize]
            public void Hook(BlubSerializer p1)
            {
            }
        }

        public class InvalidReturnModel
        {
            [BlubAfterSerialize]
            public int Hook(BlubSerializer p1, BinaryWriter p2, string p3)
            {
                return 0;
            }
        }
    }

    public static class InvalidAfterDeserializeModel
    {
        public class InvalidParameter1Model
        {
            [BlubAfterDeserialize]
            public void Hook(string p1, BinaryReader p2, string p3)
            {
            }
        }

        public class InvalidParameter2Model
        {
            [BlubAfterDeserialize]
            public void Hook(BlubSerializer p1, string p2, string p3)
            {
            }
        }

        public class InvalidParameter3Model
        {
            [BlubAfterDeserialize]
            public void Hook(BlubSerializer p1, BinaryReader p2, int p3)
            {
            }
        }

        public class InvalidParameter4Model
        {
            [BlubAfterDeserialize]
            public void Hook(BlubSerializer p1)
            {
            }
        }

        public class InvalidReturnModel
        {
            [BlubAfterDeserialize]
            public int Hook(BlubSerializer p1, BinaryReader p2, string p3)
            {
                return 0;
            }
        }
    }
}
