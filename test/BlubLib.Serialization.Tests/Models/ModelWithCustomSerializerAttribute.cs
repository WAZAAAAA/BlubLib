using BlubLib.Serialization.Tests.Serializers;

namespace BlubLib.Serialization.Tests.Models
{
    [BlubSerializer(typeof(ModelWithCustomSerializerAttributeSerializer))]
    public class ModelWithCustomSerializerAttribute
    {
        public byte A { get; set; }
    }
}
