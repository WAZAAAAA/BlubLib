using System;
using BlubLib.Serialization.Tests.Models;
using Sigil;

namespace BlubLib.Serialization.Tests.Serializers
{
    public class ModelWithCustomCompilerAttributeCompiler : ISerializerCompiler
    {
        public bool CanHandle(Type type)
        {
            return type == typeof(ModelWithCustomCompilerAttribute);
        }

        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            ctx.Emit.NewObject<ModelWithCustomCompilerAttribute>();
            ctx.Emit.StoreLocal(value);

            using (var a = ctx.Emit.DeclareLocal<byte>())
            {
                ctx.EmitDeserialize(a);

                ctx.Emit.LoadLocal(value);
                ctx.Emit.LoadLocal(a);
                ctx.Emit.Call(typeof(ModelWithCustomCompilerAttribute).GetProperty(nameof(ModelWithCustomCompilerAttribute.A)).SetMethod);
            }
        }

        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            using (var a = ctx.Emit.DeclareLocal<byte>())
            {
                ctx.Emit.LoadLocal(value);
                ctx.Emit.Call(typeof(ModelWithCustomCompilerAttribute).GetProperty(nameof(ModelWithCustomCompilerAttribute.A)).GetMethod);
                ctx.Emit.StoreLocal(a);
                ctx.EmitSerialize(a);
            }
        }
    }
}
