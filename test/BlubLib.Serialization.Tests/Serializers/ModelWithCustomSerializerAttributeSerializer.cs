using System;
using System.IO;
using BlubLib.Serialization.Tests.Models;

namespace BlubLib.Serialization.Tests.Serializers
{
    public class ModelWithCustomSerializerAttributeSerializer : ISerializer<ModelWithCustomSerializerAttribute>
    {
        public bool CanHandle(Type type)
        {
            return type == typeof(ModelWithCustomSerializerAttribute);
        }

        public void Serialize(BlubSerializer blubSerializer, BinaryWriter writer, ModelWithCustomSerializerAttribute value)
        {
            writer.Write(value.A);
        }

        public ModelWithCustomSerializerAttribute Deserialize(BlubSerializer blubSerializer, BinaryReader reader)
        {
            return new ModelWithCustomSerializerAttribute { A = reader.ReadByte() };
        }
    }
}
