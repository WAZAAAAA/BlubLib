﻿using System;
using BlubLib.Serialization.Tests.Models;
using Sigil;

namespace BlubLib.Serialization.Tests.Serializers
{
    public class SimpleModelCompiler : ISerializerCompiler
    {
        public bool CanHandle(Type type)
        {
            return type == typeof(SimpleModel);
        }

        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            using (var a = ctx.Emit.DeclareLocal<int>())
            {
                ctx.Emit.LoadLocal(value);
                ctx.Emit.Call(typeof(SimpleModel).GetProperty(nameof(SimpleModel.A)).GetMethod);
                ctx.Emit.Convert<int>();
                ctx.Emit.StoreLocal(a);
                ctx.EmitSerialize(a);
            }
        }

        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            ctx.Emit.NewObject<SimpleModel>();
            ctx.Emit.StoreLocal(value);

            using (var a = ctx.Emit.DeclareLocal<int>())
            {
                ctx.EmitDeserialize(a);

                ctx.Emit.LoadLocal(value);
                ctx.Emit.LoadLocal(a);
                ctx.Emit.Convert<byte>();
                ctx.Emit.Call(typeof(SimpleModel).GetProperty(nameof(SimpleModel.A)).SetMethod);
            }
        }
    }
}
